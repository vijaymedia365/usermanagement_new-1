import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';
//import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';

import { SettingsService } from '../../shared/data/settings.service';
import { SubscriptionService } from '../../shared/data/subscription.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  @ViewChild("placesRef") placesRef : GooglePlaceDirective;
  @ViewChild('companylogoLabel') companylogoLabel : ElementRef;
  regularForm: FormGroup;
  options = {
    types: ['geocode'],
    componentRestrictions: { country: 'AU' }
  };
  currentDate;
  userId;
  subscription;
  expiry_date;
  pendingDays;
  showSubscription: boolean = false;
  subscriptionList;
  subscribed: boolean = false;
  selectedSubcription;
  subtype;subscriptionId;

  constructor(private http: HttpClient, public subscriptionService: SubscriptionService, private router: Router, private route: ActivatedRoute, public settingsservice: SettingsService) {
    this.userId = JSON.parse(localStorage.getItem('currentUser'));
    this.subscriptionService.getSubscription().then(data => {
      this.subscriptionList = data['subscription'];
      console.log('Get Subscription list - ' + JSON.stringify(this.subscriptionList));
    });
    this.subscriptionService.getOrgSubscriptionById(this.userId['id']).then(data => {
      this.expiry_date = data['organization'][0].expiry_date;
      var x = new Date(this.expiry_date)
      var myVar = x.toDateString();
      var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
      var firstDate = new Date();
      var secondDate = new Date(myVar);
      this.pendingDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
    });
  }

  ngOnInit() {
    this.regularForm = new FormGroup({
      'webname': new FormControl(null, [Validators.required]),
      'sitedir': new FormControl(null, [Validators.required]),
      'companylogo': new FormControl(null, [Validators.required]),
      'webemail': new FormControl(null, [Validators.required]),
      'defaultcurrency': new FormControl(null, [Validators.required]),
      'payemail': new FormControl(null, [Validators.required]),
      'webphone': new FormControl(null, [Validators.required]),
      'address': new FormControl(null, [Validators.required]),
      'registrationnotification': new FormControl(null, [Validators.required]),
      'allowregistration': new FormControl(null, [Validators.required]),
      'points': new FormControl(null, [Validators.required]),
      'regverification': new FormControl(null, [Validators.required]),
      'listadmin': new FormControl(null, [Validators.required]),
      'subscriptionType': new FormControl(null, [Validators.required]),
      'pendingPeriod': new FormControl(null, [Validators.required])
    }, {updateOn: 'blur'});
      this.currentDate = Date.now();
    	this.settingsservice.getSettings().then(data => {
          this.subscriptionService.getSubscriptionByType(this.userId['subscription_type']).then(subdata => {
			if(data['status'] == 'success'){
				this.regularForm.controls['webname'].setValue(data['settings'][0].webname);
				this.regularForm.controls['sitedir'].setValue(data['settings'][0].sitedir);
				//this.regularForm.controls['companylogo'].setValue(data['settings'][0].companylogo);
				this.regularForm.controls['webemail'].setValue(data['settings'][0].webemail);
				this.regularForm.controls['payemail'].setValue(data['settings'][0].payemail);
				this.regularForm.controls['points'].setValue(data['settings'][0].points);
				//this.regularForm.controls['datetype'].setValue(data['settings'][0].datetype);
				this.regularForm.controls['allowregistration'].setValue(data['settings'][0].allowregistration);
				this.regularForm.controls['regverification'].setValue(data['settings'][0].regverification);
				this.regularForm.controls['registrationnotification'].setValue(data['settings'][0].registrationnotification);
				this.regularForm.controls['defaultcurrency'].setValue(data['settings'][0].defaultcurrency);
				this.regularForm.controls['webphone'].setValue(data['settings'][0].webphone);
				this.regularForm.controls['listadmin'].setValue(data['settings'][0].listadmin);
                this.regularForm.controls['subscriptionType'].setValue(subdata['type'][0]['type']);
                this.regularForm.controls['pendingPeriod'].setValue(this.pendingDays + ' Days');
			}
            this.subscription = subdata['type'][0]['type'];
            this.subscriptionId = subdata['type'][0]['id'];
          });
		});
  }

  handleAddressChange(address) {
    // Do some stuff
    //this.newaddress = address.formatted_address;
  }

  uploadCompanyLogo(files: File[]){
	    //pick from one of the 4 styles of file uploads below
	    //this.uploadAndProgress(files);
	    //this.settingDetail.companylogo = files[0].name;
      this.companylogoLabel.nativeElement.innerHTML = files[0].name;
      console.log(this.companylogoLabel.nativeElement);
      //alert(files[0].name);
      this.uploadAndProgress(files);
	}

  uploadAndProgress(files: File[]){
    console.log(files)
    var formData = new FormData();
    Array.from(files).forEach(f => formData.append('file',f))

    this.http.post('https:/click365.com.au/usermanagement/uploadLogo.php?fld=CompanyLogo', formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          //this.percentDone = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          //this.uploadSuccess = true;
        }
    });
  }
  
  change() {
    this.showSubscription = true;
  }

  getId(id,type){
    this.subscribed = true;
    this.selectedSubcription = type;
  }

  cancel(){
    this.showSubscription = false;
  }

  onSave(){
    this.subscriptionService.updateSubscriptionType(this.selectedSubcription,this.userId['id']).then(data => {
      if(data['status'] == 'true'){
        this.regularForm.controls['subscriptionType'].setValue(this.selectedSubcription);
        this.showSubscription = false;
      }
    });
  }

  onReactiveFormSubmit(){
    let settingDetail = {
      'webname': this.regularForm.controls['webname'].value,
  		'sitedir': this.regularForm.controls['sitedir'].value,
  		'webemail': this.regularForm.controls['webemail'].value,
  		'payemail': this.regularForm.controls['payemail'].value,
  		'defaultcurrency': this.regularForm.controls['defaultcurrency'].value,
  		'webphone': this.regularForm.controls['webphone'].value,
  		'address': this.regularForm.controls['address'].value,
  		'companylogo': this.regularForm.controls['companylogo'].value,
  		'registrationnotification': this.regularForm.controls['registrationnotification'].value,
  		'allowregistration': this.regularForm.controls['allowregistration'].value,
  		'points': this.regularForm.controls['points'].value,
  		'regverification': this.regularForm.controls['regverification'].value,
      'listadmin': this.regularForm.controls['listadmin'].value,
    };
    this.settingsservice.saveSettings(settingDetail).then(data => {
			if(data['status']){
				window.location.reload();
			}
		});
	}
  cancelEdit(){
    this.regularForm.reset();
  }
}
