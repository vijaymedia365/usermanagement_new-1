import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import swal from 'sweetalert2';

import { OrganizationService } from '../shared/data/organization.service';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss']
})
export class OrganizationComponent implements OnInit {
	organizationForm: FormGroup;
	public showOrg: boolean = false;
	//public organizations = [];
	public organizations;
	public rows;
	public newaddress;
	columns = [
        { prop: 'organizationName' },
        { prop: 'organizationEmail' },
        { prop: 'organizationPhone' },
    	{ prop: 'organizationAddress' }
    ];
    organization = {
        'id': '',
        'orgName': '',
        'orgEmail': '',
        'orgPhone': '',
        'orgAddress': ''
    };
    data = {'status': ''};

    style = 'material';
    title = 'Success';
    body = 'Organization created successfully!';
    timeout = 3000;
    position: SnotifyPosition = SnotifyPosition.centerTop;
    progressBar = true;
    closeClick = true;
    newTop = true;
    backdrop = -1;
    dockMax = 8;
    blockMax = 6;
    pauseHover = true;
    titleMaxLength = 15;
    bodyMaxLength = 80;
    public status;

    public timestamp = new Date().getTime();

	@ViewChild(DatatableComponent) table: DatatableComponent;

  	constructor(public organizationservice: OrganizationService, private snotifyService: SnotifyService) {
  		this.showOrg = false;
  		//this.status = false;
		/*this.organizationservice.getOrganization().then(data => {
			if(data['organization']){
				console.log('ORG data - ' + JSON.stringify(data['organization']));
				for (let i = 0; i < data['organization'].length; i++) {
					let array = {
						organizationName: data['organization'][i]['name'],
						organizationEmail: data['organization'][i]['email'],
						organizationPhone: data['organization'][i]['phone']
					}
					this.organizations.push(array);
				}
				this.rows = this.organizations;
			}
		});*/
  	}

  	ngOnInit() {
  		this.organizationForm = new FormGroup({
			'orgName': new FormControl(null, [Validators.required]),
			'orgEmail': new FormControl(null, [Validators.required]),
			'orgPhone': new FormControl(null, [Validators.required]),
			'orgAddress': new FormControl(null, [Validators.required])
	    });
	    this.showOrg = false;
	    console.log('timestamp- ' + this.timestamp);
	    this.organizationservice.getCharts().then(data => {
	    	console.log('getCharts - ' + JSON.stringify(data));
	    });
		this.organizationservice.getOrganization(new Date().getTime()).then(data => {
			if(data['organization']){
				this.organizations = data['organization'];
				for (let i = 0; i < data['organization'].length; i++) {
					this.status = data['organization'][i]['status'];
				}
				console.log('ORG data - ' + JSON.stringify(data['organization']));
				// for (let i = 0; i < data['organization'].length; i++) {
				// 	let array = {
				// 		organizationName: data['organization'][i]['name'],
				// 		organizationEmail: data['organization'][i]['email'],
				// 		organizationPhone: data['organization'][i]['phone'],
				// 		organizationAddress: data['organization'][i]['address']
				// 	}
				// 	this.organizations.push(array);
				// }
				// this.rows = this.organizations;
			}
		});
  	}

  	handleAddressChange(address) {
		// Do some stuff
		this.newaddress = address.formatted_address;
	}

  	getConfig(): SnotifyToastConfig {
        this.snotifyService.setDefaults({
            global: {
                newOnTop: this.newTop,
                maxAtPosition: this.blockMax,
                maxOnScreen: this.dockMax,
            }
        });
        return {
            bodyMaxLength: this.bodyMaxLength,
            titleMaxLength: this.titleMaxLength,
            backdrop: this.backdrop,
            position: this.position,
            timeout: this.timeout,
            showProgressBar: this.progressBar,
            closeOnClick: this.closeClick,
            pauseOnHover: this.pauseHover
        };
    }

  	saveOrganization(){
	  	let organizationDetail = {
			'orgName': this.organizationForm.controls['orgName'].value,
			'orgEmail': this.organizationForm.controls['orgEmail'].value,
			'orgPhone': this.organizationForm.controls['orgPhone'].value,
			'orgAddress': this.newaddress
		}
	  	this.organizationservice.saveOrganization(organizationDetail).then(data => {
	  		console.log('timestamp on sub- ' + this.timestamp);
	  		console.log('timestamp on save- ' + new Date().getTime());
    		this.organizationservice.getOrganization(new Date().getTime()).then(data => {
				if(data['organization']){
					this.organizations = [];
					for (let i = 0; i < data['organization'].length; i++) {
						let array = {
							organizationName: data['organization'][i]['name'],
							organizationEmail: data['organization'][i]['email'],
							organizationPhone: data['organization'][i]['phone'],
							organizationAddress: data['organization'][i]['address']
						}
						this.organizations.push(array);
					}
					this.rows = this.organizations;
				}
			});
	  		const { timeout, closeOnClick, ...config } = this.getConfig();
	  		this.snotifyService.confirm(this.body, this.title, {
	            ...config,
	            buttons: [
	                { text: 'Ok', action: (toast) => {
	                	this.snotifyService.remove(toast.id);
	                	this.showOrg = false;
	                	//window.location.reload();
	                } }
	            ]
        	});
	  	});
	  	//this.ngOnInit();
  	}

  	editOrganization(id){
  		this.organizationservice.getOrganizationById(id).then(data => {
  			//console.log('getOrganizationById - ' + JSON.stringify(data));
            this.organization.id = data['organization'][0].id;
            this.organization.orgName = data['organization'][0].name;
            this.organization.orgEmail = data['organization'][0].email;
            this.organization.orgPhone = data['organization'][0].phone;
            this.organization.orgAddress = data['organization'][0].address;

            this.showOrg = true;
        },
        error => {
        });
    }

    viewOrganization(id) {
    	this.organizationservice.getOrganizationById(id).then(data => {
    		swal({
		        title: '<b>Organization Detail</b>',
		        type: 'info',
		        html:
		        '<table class="viewOrg"><tbody><tr></tr>' +
		        '<tr><th>ORGANIZATION NAME</th><td>'+data['organization'][0].name+'</td></tr>' +
		        '<tr><th>EMAIL</th><td>'+data['organization'][0].email+'</td></tr>' +
		        '<tr><th>PHONE</th><td>'+data['organization'][0].phone+'</td></tr>' +
		        '<tr><th>ADDRESS</th><td>'+data['organization'][0].address+'</td></tr>' +
		        '</tbody></table><br />',
		        showCloseButton: true,
		        confirmButtonText:
		        '<i class="fa fa-thumbs-up"></i> Okay!'
		    })
    	});
    }

  	onChange(status,id) {
  		this.organizationservice.updateStatus(status.toString(),id).then(data => {
  			console.log('updateStatus - ' + JSON.stringify(data));
  		});
  	}

  	createOrg(){
      	this.showOrg = true;
  	}

    cancel(){
      this.showOrg = false;
    }

}
