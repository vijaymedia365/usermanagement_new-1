import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RenewalsComponent } from "./renewals.component";

const routes: Routes = [
    {
        // path: '',
        // children: [{
        //     path: 'cards',
        //     component: CardsComponent
        // }
        // ]
        path: '',
        component: RenewalsComponent,
       data: {
         title: 'Renewals'
       },
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class RenewalsRoutingModule { }