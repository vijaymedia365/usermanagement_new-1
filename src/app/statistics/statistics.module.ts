import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ChartistModule } from 'ng-chartist';
import { StatisticsRoutingModule } from "./statistics-routing.module";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatchHeightModule } from "../shared/directives/match-height.directive";
import { StatisticsComponent } from "./statistics.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        StatisticsRoutingModule,
        NgxChartsModule,
        ChartistModule,
        NgbModule,
        MatchHeightModule,
        FormsModule,
        ReactiveFormsModule,
        NgxDatatableModule
    ],
    declarations: [
        StatisticsComponent,
   ]
})
export class StatisticsModule { }
